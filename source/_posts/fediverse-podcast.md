
---
layout: "post"
title: "FediverseCast"
date: 2018-09-19
tags:
    - fediverse
preview: "Announcing the first episode of a new podcast about Fediverse! The show will feature conversations with  fediverse makers and community members"
url: "https://fediversecast.com"
lang: en
---

Announcing the [first episode](https://fediversecast.com/2018/09/18/fediversecast-episode-1) of a new podcast about Fediverse! The show will feature conversations with Fediverse makers and community members. The first episode is about [Funkwhale](https://join.funkwhale.audio), software for hosting and sharing music, in active development and soon to federate with other networks.
