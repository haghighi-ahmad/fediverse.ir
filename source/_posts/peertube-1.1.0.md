---
layout: "post"
title: "PeerTube 1.1.0"
date: 2018-12-05
tags:
    - peertube
preview: "PeerTube new release. Featuring speedier startup, PeerTube CLI, bulk actions, ability to list all local videos, muting accounts and much more"
url: "https://github.com/Chocobozzz/PeerTube/releases.atom"
lang: en
---

PeerTube 1.1.0 release comes with the following improvements: CLI to run PeerTube without client, speeding up PeerTube startup, ability to disable webtorrent, admin email verification, ability for users and administrators to mute an account or an instance, moderation tools in the account page, bulk actions (delete / ban) in users table, search filter in admin following, followers and users table, ability to list all local videos.

Full [changelog](https://github.com/Chocobozzz/PeerTube/releases.atom)
